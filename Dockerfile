FROM python:3.11.1-slim-bullseye

ARG RDBMS
ARG DB_HOST
ARG DB_NAME
ARG DB_PORT
ARG TABLE_NAME
ARG PG_SCHEMA

ARG DB_USER
ARG DB_PASSWORD

ENV RDBMS=$RDBMS
ENV DB_HOST=$DB_HOST
ENV DB_NAME=$DB_NAME
ENV DB_PORT=$DB_PORT
ENV TABLE_NAME=$TABLE_NAME
ENV PG_SCHEMA=$PG_SCHEMA

ENV DB_USER=$DB_USER
ENV DB_PASSWORD=$DB_PASSWORD

RUN apt-get update && \
    apt-get install -y gcc \
                        default-libmysqlclient-dev && \
    apt-get -qq -y autoremove && \
    apt-get autoclean && \
    rm -rf /var/lib/apt/lists/* /var/log/dpkg.log

RUN pip install --upgrade pip
COPY requirements.txt /
RUN pip install -r /requirements.txt

COPY ./jobs/* .

CMD ["python", "./update_fingerprints_table.py"]