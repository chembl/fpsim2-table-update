from FPSim2.io import create_db_table
from sqlalchemy import create_engine, text
from rdkit import RDLogger
import os

# Disable RDKit warnings
RDLogger.DisableLog('rdApp.*')

rdbms = os.getenv("RDBMS")
db_host = os.getenv("DB_HOST")
db_port = os.getenv("DB_PORT")
db_name = os.getenv("DB_NAME")
table_name = os.getenv("TABLE_NAME")
db_user = os.getenv("DB_USER")
db_password = os.getenv("DB_PASSWORD")

mol_format = os.getenv("MOL_FORMAT")
fp_type = os.getenv("FP_TYPE")
fp_radius = os.getenv("FP_RADIUS")
fp_bits = os.getenv("FP_BITS")
new_cpds_query = os.getenv("NEW_CPDS_QUERY")


if rdbms.startswith("oracle"):
    url = f"oracle+oracledb://{db_user}:{db_password}@{db_host}:{db_port}/?service_name={db_name}"
else:
    url = f"{rdbms}://{db_user}:{db_password}@{db_host}:{db_port}/{db_name}"

engine = create_engine(url)
with engine.connect() as conn:
    create_db_table([], url, table_name, mol_format, fp_type, {"radius": int(fp_radius), "fpSize": int(fp_bits)})
    res = conn.execute(text(new_cpds_query))
    create_db_table(res, url, table_name, mol_format, fp_type, {"radius": int(fp_radius), "fpSize": int(fp_bits)})
